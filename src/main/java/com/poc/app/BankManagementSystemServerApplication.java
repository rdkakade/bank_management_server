package com.poc.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankManagementSystemServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankManagementSystemServerApplication.class, args);
	}

}
